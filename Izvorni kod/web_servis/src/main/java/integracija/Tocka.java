package integracija;

public class Tocka {
	
	private float x, y;

	public Tocka(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float X() {
		return this.x;
	}
	
	public float Y() {
		return this.y;
	}
	
}
