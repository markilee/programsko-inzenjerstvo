package integracija;

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		
		// Inicijalizacija objekta koji će se koristiti za komunikaciju s web servisom
		Klasifikator_analizator sucelje = new Klasifikator_analizator(/* Ovdje će još ići neki argumenti poput puta do datoteke s podacima i slično */);
		sucelje.obaviSve("DummyKlasifikator" /* Prosljeđuje se klasifikator koji se želi koristiti */);
		
		Tocka kriticnaTocka = sucelje.vratiKriticnuTocku(); // Servis vraća objekt klase Tocka koji predstavlja kritičnu točku na grafu
		ArrayList<Tocka> tocke = sucelje.vratiTocke(); // Servis vraća listu objekata Tocka koja predstavlja sve točke u grafu
		
		System.out.println("Kritična točka (x, y): (" + kriticnaTocka.X() + ", " + kriticnaTocka.Y() + ")"); // Ispis kritične točke
		System.out.println(); // Red razmaka
		
		// Ispis svih točaka
		for (int i = 0; i < tocke.size(); i++) {
			System.out.println("Točka " + (i+1) + " (x, y): (" + tocke.get(i).X() + ", " + tocke.get(i).Y() + ")");
		}
	}

}
