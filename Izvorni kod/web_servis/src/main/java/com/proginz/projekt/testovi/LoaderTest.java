package com.proginz.projekt.testovi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

import com.proginz.projekt.web_servis.Loader;

import junit.framework.TestCase;

public class LoaderTest extends TestCase {
	
	
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testDefaultDataset() {
		Loader loader;
		try {
			loader = new Loader(-1);
			String val = loader.vratiPutDoDatoteke();
			if(!val.endsWith(".csv"))fail("Nije .csv datoteka");
		} catch (NumberFormatException e) {
			fail("Defaultna datoteka je neispravnog formata");
		} catch (IOException e) {
			fail("Nepostojeća datoteka");
		}
		
	}
	
	public void testCustomDataset() {
		InputStream in = this.getClass().getResourceAsStream("/JDT_R2_1.csv");
		File temp;
		try {
			temp = File.createTempFile("temp-file-dataset-" + System.currentTimeMillis(), ".csv");
			Files.copy(in, Paths.get(temp.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
			Loader loader = new Loader( temp.getAbsolutePath() );;
			
			/* Provjeri put do novostvorene datoteke */
			String path = loader.vratiPutDoDatoteke();
			if(path.length() <= 0)fail("Put do datoteke je neispravan");
			
		} catch (IOException e1) {
			fail("Loader ima probleme sa čitanjem/stvaranjem CSV datoteke. Provjerite dozvole.");
		} catch (NumberFormatException e) {
			fail("Loader je pronašao grešku u ispravnom datasetu. Loader je neispravan");
		}
	}
	
	public void testCustomCorruptedDataset() {
		InputStream in = this.getClass().getResourceAsStream("/JDT_R2_1_corrupted.csv");

		File temp;
		try {
			temp = File.createTempFile("temp-file-dataset-" + System.currentTimeMillis(), ".csv");
			Files.copy(in, Paths.get(temp.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
			Loader loader = new Loader();
			loader.ucitajCSV( temp.getAbsolutePath() );
		} catch (IOException e1) {
			fail("Loader ima probleme sa čitanjem/stvaranjem CSV datoteke.");
		} catch (NumberFormatException e) {
			return;			// Ovo je ispravni ishod testa
		}

		fail("Loader nije prepoznao pogrešku u datasetu.");
	}
	
	

}
