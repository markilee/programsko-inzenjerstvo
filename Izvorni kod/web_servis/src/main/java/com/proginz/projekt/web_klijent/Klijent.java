package com.proginz.projekt.web_klijent;

import java.util.ArrayList;
import java.util.Arrays;

import com.proginz.projekt.web_servis.Klasifikator_analizator;
import com.proginz.projekt.web_servis.Tocka;
import com.proginz.projekt.web_klijent.GUI;

public class Klijent {
	public static void main(String[] args) throws Exception {
		
		//Klasifikator_analizator sucelje = new Klasifikator_analizator("C:/Users/marko-pc/Desktop/kiki/JDT_R3_1.csv", "http://192.168.43.85:7779/ws/sucelje?wsdl");
		Klasifikator_analizator sucelje = new Klasifikator_analizator(1, "http://localhost:7779/ws/sucelje?wsdl");
		sucelje.obaviSve("NaiveBayes");
		ArrayList<Tocka> arr = sucelje.vratiTocke();
		for (int i=0; i<arr.size(); i++) {
			System.out.println("("+arr.get(i).X()+", "+arr.get(i).Y()+")");
		}
        double[] coeff = sucelje.vratiRegresiju();
        System.out.println("coef="+Arrays.toString(coeff));
        System.out.println(sucelje.granicnaRazinaNeujednacenosti(coeff));
        Tocka t = sucelje.vratiTockuGranicneRazineNeujednacenosti();
        System.out.println(t.X() + ", " + t.Y());
        
	}  
}
