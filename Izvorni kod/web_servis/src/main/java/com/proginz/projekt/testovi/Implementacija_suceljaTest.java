package com.proginz.projekt.testovi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.proginz.projekt.web_servis.Implementacija_sucelja;

import junit.framework.TestCase;

public class Implementacija_suceljaTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testUvozPodatakaString() {
		Implementacija_sucelja imp = new Implementacija_sucelja();
		
	    /* Slanje podataka */
	    try {
			imp.uveziPodatkeString( TestHelper.resourceToTempFile("/mali.csv").getAbsolutePath() );
		} catch (NumberFormatException e) {
			fail("Neispravan format vrijednosti u datasetu");
		} catch (IOException e) {
			fail("IO pogreška");
		}
	}
	
	public void testUvozPodatakaInt() {
		Implementacija_sucelja imp = new Implementacija_sucelja();
	    
	    /* Zahtjevanje čitanja lokalnog dataseta sa servera */
	    try {
			imp.uveziPodatkeInt(0);
		} catch (NumberFormatException e) {
			fail("Neispravan format vrijednosti u datasetu");
		} catch (IOException e) {
			fail("IO pogreška");
		}
	}
	
	public void testPodijeliPaEvaluiraj() {
		Implementacija_sucelja imp = new Implementacija_sucelja();
		
		/* Zahtjevanje čitanja lokalnog dataseta sa servera */
	    try {
			imp.uveziPodatkeInt(2);
		} catch (NumberFormatException e) {
			fail("Neispravan format vrijednosti u datasetu");
		} catch (IOException e) {
			fail("IO pogreška");
		}
	    
	    try {
			imp.podijeliPaEvaluiraj("NaiveBayes");
		} catch (Exception e) {
			fail("Dogodila se pogreška tijekom podjele dataseta ili treniranja modela");
		}
		
	}

}
