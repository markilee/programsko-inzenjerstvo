package com.proginz.projekt.testovi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class TestHelper {
	public static File resourceToTempFile(String path) throws IOException {
		File temp = File.createTempFile("temp-file-dataset-" + System.currentTimeMillis(), ".csv");
		InputStream in = TestHelper.class.getResourceAsStream(path);
		Files.copy(in, Paths.get(temp.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
		return temp;
	}
}
