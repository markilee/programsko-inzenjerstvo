package com.proginz.projekt.web_servis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.io.PrintWriter;

public class Loader {
	
	private ArrayList<ArrayList<Double>> podaci;
	private int minBug, maxBug, _odBug, _doBug;
	private String putDoDatoteke;
	String put = "";
	
	public Loader() {
	}

	public Loader(int brojPrimjera) throws NumberFormatException, IOException {
		switch (brojPrimjera) {
			case 1:
				put = "/JDT_R2_0.csv";
				break;
			
			case 2:
				put = "/mali.csv";
				break;
				
			case 3:
				put = "/JDT_R2_1.csv";
				break;
				
			case 4:
				put = "/JDT_R3_0.csv";
				break;
				
			case 5:
				put = "/JDT_R3_1.csv";
				break;
				
			case 6:
				put = "/JDT_R3_2.csv";
				break;
	
			default:
				put = "/JDT_R2_0.csv";
				break;
		}
		
		put = resourceToTmpFile(put);
		ucitajCSV(put);
	}
	
	public Loader(String podaci) throws IOException {
		stvoriCSV(podaci);
		ucitajCSV(put);
	}
	
	private String resourceToTmpFile(String put) throws IOException {
		File temp = File.createTempFile("temp-file-dataset-" + System.currentTimeMillis(), ".csv");
		InputStream in = this.getClass().getResourceAsStream(put);
		Files.copy(in, Paths.get(temp.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
		return temp.getAbsolutePath();
	}
	
	private void stvoriCSV(String podaci) throws IOException {
		File temp = File.createTempFile("temp-file-dataset-" + System.currentTimeMillis(), ".csv");
		try (PrintWriter ispis = new PrintWriter(temp)) {
		    ispis.print(podaci);
		}
		this.put = temp.getAbsolutePath();
	}
	
	public void ucitajCSV(String put) throws IOException {
		
		putDoDatoteke = put;
		BufferedReader citac = new BufferedReader(new FileReader(put));
		String linija = "";
		podaci = new ArrayList<ArrayList<Double>>();
		boolean prvi = true;
		minBug = -1;
		maxBug = -1;
		int brojBugova;
		
		while ((linija = citac.readLine()) != null) {
			
			if (prvi) {
				prvi = false;
				continue;
			}
			
			String[] odvojeniPodaci = linija.split(",");
			ArrayList<Double> redak = new ArrayList<Double>();
			for (int i=0; i<odvojeniPodaci.length-1; i++) {
				redak.add(Double.parseDouble(odvojeniPodaci[i+1]));
			}
			podaci.add(redak);
			brojBugova = redak.get(redak.size()-1).intValue();
			if (minBug == -1 && maxBug == -1) {
				minBug = brojBugova;
				maxBug = brojBugova;
			} else {
				minBug = (brojBugova < minBug) ? brojBugova : minBug;
				maxBug = (brojBugova > maxBug) ? brojBugova : maxBug;
			}
		}
		citac.close();
		
		int br = -1;
        while (vratiPostotakManjinskeKlase(++br)>0.5 && br>=minBug && br<=maxBug);
        _odBug = br+1;
        while (vratiPostotakManjinskeKlase(++br)>0.01 && br>=minBug && br<=maxBug);
        _doBug = br-1;
			
	}
	
	/**
	 * @param granicaBrojaBugova
	 * granica za određivanje manjinske klase, sve > granici smatra se manjinskom klasom
	 */
	public double vratiPostotakManjinskeKlase(int granicaBrojaBugova) {
		int brojPodatakaIspodGranice = 0;
		for (int i = 0; i < podaci.size(); i++) {
			if (podaci.get(i).get(podaci.get(i).size()-1) > granicaBrojaBugova) brojPodatakaIspodGranice++;
		}
		return ((double)brojPodatakaIspodGranice/podaci.size());
	}
	
	public ArrayList<Integer> vratiListuBugovaZaIteraciju() {
		double zadnjiPostotak = -1;
		double postotak = -1;
		ArrayList<Integer> nizIndexa = new ArrayList<Integer>();
		for (int i=_odBug; i<=_doBug; i++) {
			postotak = vratiPostotakManjinskeKlase(i);
			if (postotak != zadnjiPostotak) {
				nizIndexa.add(i);
				zadnjiPostotak = postotak;
			}
		}
		return nizIndexa;
	}
	
	public ArrayList<ArrayList<Double>> vratiPodatke() {
		return podaci;
	}
	
	public String vratiPutDoDatoteke() {
		return putDoDatoteke;
	}
	
}
