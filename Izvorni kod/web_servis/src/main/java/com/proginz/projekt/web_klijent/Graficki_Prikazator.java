package com.proginz.projekt.web_klijent;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.annotations.XYBoxAnnotation;
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.proginz.projekt.web_servis.Klasifikator_analizator;
import com.proginz.projekt.web_servis.Tocka;

public class Graficki_Prikazator extends JPanel {
	private double[] coeff;
	private ArrayList<Tocka> arr;
	private Tocka t;
	
	public Graficki_Prikazator(double[] coeff, ArrayList<Tocka> arr, Tocka t){
		this.coeff = coeff;
		this.arr = arr;
		this.t = t;
	}

	private double f(double x) {
		double ret = 0, X = 1;
		for (int i = 0; i < coeff.length; ++i) {
			ret += X*coeff[i];
			X *= x;
		}
		return ret;
	}
	
	public String posaraj() throws IOException {
		final XYSeries data = new XYSeries("Data");
		double maxX = -1234234234, minX = 41234123;
		for (int i = 0; i < arr.size(); ++i) {
			data.add(arr.get(i).X(), arr.get(i).Y());
			maxX = Math.max(maxX, arr.get(i).X());
			minX = Math.min(minX, arr.get(i).X());
		}
		
		final XYSeries aprox = new XYSeries("Regresija");
		for (int i = 0; i < 500; ++i) {
			double x = (maxX-minX)/500*i;
			aprox.add(x, f(x));
		}
		
		final XYSeries tockica = new XYSeries("Tockica");
		for (int i = -25; i < 25; ++i) {
			double pomak = i/500.0;
			tockica.add(t.X()+pomak/10, t.Y()+pomak);
		}
		
		final XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(data);
		dataset.addSeries(aprox);
		dataset.addSeries(tockica);
		
		JFreeChart graf = ChartFactory.createXYLineChart(
			"nez",
			"x",
			"y",
			dataset,
			PlotOrientation.VERTICAL,
			true, true, false);
		
		
		int width = 640, height = 480;
		File slikica = new File("slikica.png");
		ChartUtilities.saveChartAsPNG(slikica, graf, width, height);
		return "slikica.png";
	}
}
