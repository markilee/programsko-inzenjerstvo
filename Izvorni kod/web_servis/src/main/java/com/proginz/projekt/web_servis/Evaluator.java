package com.proginz.projekt.web_servis;

import java.util.ArrayList;
import java.util.Random;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.Logistic;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;

public class Evaluator {
	private ArrayList<ArrayList<Double>> podaci;
	private double GM;
	private AbstractClassifier model;
	private Instances dataset;
	private Evaluation eval;
	private double avgGM = 0;
	
	public Evaluator(Instances dataset, String imeModela) throws Exception {
		 this.dataset = dataset;
		 int runs = 10;
		 int seed = 1;
		  postavi_model(imeModela);
		 for (int i = 0; i < runs; i++) {
		   seed = i+1; 
		   eval = new Evaluation(dataset);
		   try {
				eval.crossValidateModel(model, dataset, 10, new Random(seed));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   System.out.println("Estimated Accuracy: "+Double.toString(eval.pctCorrect()));
		   GM = Math.sqrt( this.vrati_TPR() * this.vrati_TNR() );
		   avgGM += GM; //eval.pctCorrect();
		 }
		 avgGM = avgGM / runs;
	}
	
	private double confusionMatrixSum() {
		double m[][] = (this.eval).confusionMatrix();
		double ret = 0;
		for(int i=0; i < m.length; i++) {
			for(int j=0; j < m.length; j++) {
				ret = ret + m[i][j];
			}
		}
		return ret;
	}
	
	public double vrati_TP() {
		return (this.eval).confusionMatrix()[0][0];
	}
	
	public double vrati_TN() {
		return (this.eval).confusionMatrix()[1][1];
	}
	
	public double vrati_FN() {
		return (this.eval).confusionMatrix()[1][0];
	}
	
	public double vrati_FP() {
		return (this.eval).confusionMatrix()[0][1];
	}
	
	public double vrati_TPR() {
		return vrati_TP() / (vrati_TP() + vrati_FN());
	}
			
	public double vrati_TNR() {
		return vrati_TN() / (vrati_TN() + vrati_FP());
	}
	
	public double vrati_GM() {
		return avgGM;
	}
	
	public void postavi_model(String ime) {
		if (ime.equals("NaiveBayes")) {
			model = new NaiveBayes();
		} else if (ime.equals("J48")) {
			model = new J48();
		} else if (ime.equals("RandomTree")) {
			model = new RandomTree();
		} else if (ime.equals("RandomForest")) {
			model = new RandomForest();
		} else if (ime.equals("PART")) {
			model = new PART();
		} else if (ime.equals("Logistic")) {
			model = new Logistic();
		} else {
			model = new NaiveBayes();
		}
	}	
	
}
