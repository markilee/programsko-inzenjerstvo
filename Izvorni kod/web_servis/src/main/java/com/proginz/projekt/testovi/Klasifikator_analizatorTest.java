package com.proginz.projekt.testovi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.proginz.projekt.web_servis.Klasifikator_analizator;
import com.proginz.projekt.web_servis.Tocka;

import junit.framework.TestCase;

public class Klasifikator_analizatorTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testTocke() {
		Klasifikator_analizator sucelje;
		try {
			sucelje = new Klasifikator_analizator(2, "http://localhost:7779/ws/sucelje?wsdl");
			sucelje.obaviSve("Logistic");
			ArrayList<Tocka> arr = sucelje.vratiTocke();
			if(arr == null || arr.size() == 0)fail("Servis nije vratio točke");
		} catch (Exception e) {
			fail("Je li servis (server) dostupan?");
		}
	}
	
	public void testRegresija() {
		Klasifikator_analizator sucelje;
		try {
			sucelje = new Klasifikator_analizator(2, "http://localhost:7779/ws/sucelje?wsdl");
			sucelje.obaviSve("Logistic");
			
			/* Test regresije */
			double coef[] = sucelje.vratiRegresiju();
			if(coef == null || coef.length == 0)fail("Regresija nije vracena");
		} catch (Exception e) {
			fail("Je li servis (server) dostupan?");
		}
	}
	
	public void testGranicnaRazina() {
		Klasifikator_analizator sucelje;
		try {
			sucelje = new Klasifikator_analizator(2, "http://localhost:7779/ws/sucelje?wsdl");
			sucelje.obaviSve("RandomForest");
			
			/* Test granične točke neujednačenosti */
			Tocka t = sucelje.vratiTockuGranicneRazineNeujednacenosti();
			if(t == null)fail("Servis nije vratio granicnu tocku neujednacenosti");
			
		} catch (Exception e) {
			fail("Je li servis (server) dostupan?");
		}
	}
	
	

}
