package com.proginz.projekt.web_servis;

public class Tocka {
	private double x;
	private double y;
	
	public Tocka(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double X () {
		return this.x;
	}
	
	public double Y() {
		return this.y;
	}
}
