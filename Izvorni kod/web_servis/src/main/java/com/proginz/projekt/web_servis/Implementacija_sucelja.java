package com.proginz.projekt.web_servis;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.jws.WebService;

import weka.classifiers.AbstractClassifier;  
 
@WebService(endpointInterface = "com.proginz.projekt.web_servis.Web_sucelje")  

public class Implementacija_sucelja implements Web_sucelje {
	
	Loader citac = null;
	Divider razdvajac = null;
	Evaluator eval = null;

	private ArrayList<Double> prosjecniGM, postociManjinskeKlase = new ArrayList<Double>();
	private ArrayList<Integer> pragovi;
	
	public void uveziPodatkeString(String podaci) throws NumberFormatException, IOException {
		citac = new Loader(podaci);
	}
	
	public void uveziPodatkeInt(int brojTesta) throws NumberFormatException, IOException {
		citac = new Loader(brojTesta);
	}

	public void podijeliPaEvaluiraj(String imeModela) throws Exception {
		this.prosjecniGM = new ArrayList<Double>();
		//try {
			razdvajac = new Divider(citac.vratiPutDoDatoteke());
		/*} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		this.pragovi = citac.vratiListuBugovaZaIteraciju();
		this.postociManjinskeKlase.clear(); 
		for (Integer prag: citac.vratiListuBugovaZaIteraciju()) {
            //try {
				this.postociManjinskeKlase.add(citac.vratiPostotakManjinskeKlase(prag));
				razdvajac.razdvoji(prag);
				Random rnd = new Random(23);
				System.out.println("Prag je: " + Integer.toString(prag));
				//eval = new Evaluator(razdvajac.vratiPodatkeZaCrossValidaciju(), 10, rnd);
				eval = new Evaluator(razdvajac.vratiPodatkeZaCrossValidaciju(), imeModela);					// fix
				//postaviKlasifikator("NaiveBayes");
				//eval.postavi_model(model);
				prosjecniGM.add(eval.vrati_GM());
			/*} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	System.out.println(prag);
        }
		
	}


	public String vratiProsjecniGM () {
		return prosjecniGM.toString();
	}

	public String vratiPostotakManjinskeKlase () {
		return this.postociManjinskeKlase.toString();
	}

}
