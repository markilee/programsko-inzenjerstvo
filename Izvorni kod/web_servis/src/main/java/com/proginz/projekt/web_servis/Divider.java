package com.proginz.projekt.web_servis;

import java.util.ArrayList;
import java.util.Random;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.core.Attribute;
import weka.core.DenseInstance;

public class Divider {
	private String put;
	private Instances stratificiraniPodaci;
	
	public Divider(String put) {
		this.put = put;
	}
    
	public void razdvoji(int prag) throws Exception {
		//ucitavanje datoteke
		DataSource izvor = new DataSource(put);
		
		//uzimamo podatke za dataset iz datoteke
		Instances instances = izvor.getDataSet();
		Instances kopija = new Instances(instances);
        
		int numInstances = instances.numInstances();
		int numAttrs = instances.numAttributes();
		
		ArrayList<String> values = new ArrayList(); 
        values.add("da");             
        values.add("ne");
        kopija.insertAttributeAt(new Attribute("Klasa", values), kopija.numAttributes());
        System.out.println(kopija.numInstances());
        for (int i = 0; i < kopija.numInstances(); i++) {
            if (kopija.instance(i).value(kopija.numAttributes() - 2) > prag) { 
            	kopija.instance(i).setValue(kopija.numAttributes() - 1, "da");
            } else {
            	kopija.instance(i).setValue(kopija.numAttributes() - 1, "ne");
            }
        }

        //micanje atributa nepotrebnim za treniranje: ime datoteke i broj bugova
		Remove remove = new Remove();                // novi filter
		remove.setAttributeIndices("1," + Integer.toString(kopija.numAttributes()-1));     // namjestanje opcija
		remove.setInputFormat(kopija);               //informiranje filtera o datasetu nakon sto smo namjestili opcije
		kopija = Filter.useFilter(kopija, remove);   // primjena filtera
		
		Random rand = new Random(System.currentTimeMillis());   //seed za generiranje slucajnih brojeva
		//zadnji atribut se postavlja kao klasa
		kopija.setClassIndex(kopija.numAttributes() - 1);	
		kopija.randomize(rand);         // dataset se slučajno raspodijeli
		kopija.stratify(10);            // datasetse razdvaja na 10 dijelova
        
		//System.out.println(kopija);
		stratificiraniPodaci = kopija;
    }
	
	public Instances vratiPodatkeZaCrossValidaciju() {
		return stratificiraniPodaci;
	}
			    
}
