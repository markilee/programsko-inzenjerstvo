package com.proginz.projekt.web_klijent;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.proginz.projekt.web_servis.Klasifikator_analizator;
import com.proginz.projekt.web_servis.Tocka;

public class GUI extends JFrame implements ActionListener {
	private String filedir = "null";
	private JFileChooser datoteka = new JFileChooser(); 
	private ButtonGroup modeli;
	private Graficki_Prikazator gp;
	private JLabel graf;
	private JPanel ulazniPodaci = new JPanel();
	
	public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return button.getText();
            }
        }
        return null;
    }
	
	public GUI() throws IOException {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(570, 400);
		this.setTitle("Grafički Prikazator");
		
		JButton opriFajl = new JButton("Load");
		opriFajl.setSize(50, 12);
		opriFajl.setActionCommand("oprifajl");
		opriFajl.addActionListener(this);
		
		JPanel radios = new JPanel();
		JRadioButton NaiveBayes = new JRadioButton("NaiveBayes");
		JRadioButton J48 = new JRadioButton("J48");
		JRadioButton RandomTree = new JRadioButton("RandomTree");
		JRadioButton RandomForest = new JRadioButton("RandomForest");
		JRadioButton PART = new JRadioButton("PART");
		JRadioButton Logistic = new JRadioButton("Logistic");
		
		modeli = new ButtonGroup();
		modeli.add(NaiveBayes);
		modeli.add(J48);
		modeli.add(RandomTree);
		modeli.add(RandomForest);
		modeli.add(PART);
		modeli.add(Logistic);
		
		radios.add(NaiveBayes);
		radios.add(J48);
		radios.add(RandomTree);
		radios.add(RandomForest);
		radios.add(PART);
		radios.add(Logistic);
		
		NaiveBayes.setSelected(true);
		
		JButton okurblaj = new JButton("Okurblaj");
		okurblaj.setSize(50, 12);
		okurblaj.setActionCommand("okurblaj");
		okurblaj.addActionListener(this);
		
		ulazniPodaci.add(opriFajl);
		ulazniPodaci.add(radios);
		ulazniPodaci.add(okurblaj);
		
		JPanel canvas = new JPanel();
		
		graf = new JLabel();
		ulazniPodaci.add(graf);
		
		this.add(ulazniPodaci);
		
		this.setResizable(false);
		this.setVisible(true);
		
	}
	
	private void okurblaj() throws Exception {
		//Klasifikator_analizator sucelje = new Klasifikator_analizator("C:/Users/marko-pc/Desktop/kiki/JDT_R3_1.csv", "http://192.168.43.85:7779/ws/sucelje?wsdl");
		Klasifikator_analizator sucelje;
		if ("null".equals(filedir)) {
			sucelje = new Klasifikator_analizator(1, "http://localhost:7779/ws/sucelje?wsdl");
		} else {
			sucelje = new Klasifikator_analizator(filedir, "http://localhost:7779/ws/sucelje?wsdl");
		}
		sucelje.obaviSve(getSelectedButtonText(modeli));
		ArrayList<Tocka> arr = sucelje.vratiTocke();
		Graficki_Prikazator gp = new Graficki_Prikazator(sucelje.vratiRegresiju(), sucelje.vratiTocke(), sucelje.vratiTockuGranicneRazineNeujednacenosti());
		String slikica = gp.posaraj();
		ImageIcon icon = new ImageIcon(slikica);
        graf.setIcon(new ImageIcon(icon.getImage().getScaledInstance(400, 300, java.awt.Image.SCALE_SMOOTH)));
		for (int i=0; i<arr.size(); i++) {
			System.out.println("("+arr.get(i).X()+", "+arr.get(i).Y()+")");
		}
        double[] coeff = sucelje.vratiRegresiju();
        System.out.println("coef="+Arrays.toString(coeff));
        System.out.println(sucelje.granicnaRazinaNeujednacenosti(coeff));
        Tocka t = sucelje.vratiTockuGranicneRazineNeujednacenosti();
        System.out.println(t.X() + ", " + t.Y());
	}
	
	public void actionPerformed(ActionEvent e) {
		if ("oprifajl".equals(e.getActionCommand())) {
			datoteka.setCurrentDirectory(new File(System.getProperty("user.home")));
			int result = datoteka.showOpenDialog(this);
			if (result == datoteka.APPROVE_OPTION) {
				this.filedir = datoteka.getSelectedFile().getAbsolutePath();
				System.out.println(this.filedir);
			}
		} else if ("okurblaj".equals(e.getActionCommand())) {
			try {
				okurblaj();
			} catch (Exception e1) {
				// marko zasto throwate exceptione da moram ova smeca stavljati
				JOptionPane.showMessageDialog(null, "Neispravan CSV ili greška u radu servisa", "Pogreška", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	public String getFiledir() {
		return this.filedir;
	}
}
