package com.proginz.projekt.testovi;

import com.proginz.projekt.web_servis.Divider;

import junit.framework.TestCase;
import weka.core.Instance;
import weka.core.Instances;

public class DividerTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	public void testDividerNumClasses() {
		Divider divider = null;
		try {
			divider = new Divider( TestHelper.resourceToTempFile("/mali.csv").getAbsolutePath() );
			divider.razdvoji(250);
		} catch (Exception e) {
			fail("Greška u obradi dataseta");
		}
		
		Instances i = divider.vratiPodatkeZaCrossValidaciju();
		int num = i.numClasses();
		
		if(num != 2)fail("Broj klasa je različit od 2 (broj klasa = " + Integer.toString(num) + ")");
	}
	
	public void testDividerPodjela() {
		Divider divider = null;
		try {
			divider = new Divider( TestHelper.resourceToTempFile("/mali.csv").getAbsolutePath() );
			divider.razdvoji(30);
		} catch (Exception e) {
			fail("Greška u obradi dataseta");
		}
		
		Instances instances = divider.vratiPodatkeZaCrossValidaciju();
		int arr[] = new int[2];
		
		for(int i=0; i < instances.numInstances(); i++) {
			Instance instance = instances.instance(i);
			double c = instance.classValue();
			arr[(int)c]++;
		}
		
		if(arr[0] != 7 || arr[1] != 3) {
			fail("Dataset je krivo podijeljen, klasa 0 se spominje " + arr[0] + " puta, a klasa 1 " + arr[1] + " puta.");
		}
	}

}
