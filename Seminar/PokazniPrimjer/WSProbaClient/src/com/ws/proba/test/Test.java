package com.ws.proba.test;

import java.rmi.RemoteException;

import com.ws.proba.MojaKlasaStub;
import com.ws.proba.MojaKlasaStub.MojBrojResponse;

public class Test {

	public static void main(String[] args) {
		System.out.println("Pokrenut");
		try {
			MojaKlasaStub klasa = new MojaKlasaStub();
			MojaKlasaStub.MojBroj mojBroj = new MojaKlasaStub.MojBroj();
			mojBroj.setParam(15);
			
			MojBrojResponse rez = klasa.mojBroj(mojBroj);
			System.out.println(rez.get_return());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
